
(function (angular) {

	function MainController($scope, $route, $sce, $log) {
		this.text = 'This is the initial text';
	}

	angular.extend(MainController.prototype, {
		test: function () {
			this.text += ' TEST';
		}
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider, $sceProvider) {
		$sceProvider.enabled(false);

		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');
	})

	;

})(angular);
