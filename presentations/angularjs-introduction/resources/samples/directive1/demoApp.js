
(function (angular) {

	

	/////

	function MainController() {
		this.text = 'Hello World!';
	}

	angular.extend(MainController.prototype, {
		doStuff: function () {
			this.text += ' doing stuff';
		},
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');
	})



	.directive('demoDirective', function () {
		return {
			templateUrl: 'demo-directive.view.html',
			// scope: {
			// 	text: '=',
			// 	text2: '&',
			// 	text3: '@',
			// },
		};
	})

	;

})(angular);
