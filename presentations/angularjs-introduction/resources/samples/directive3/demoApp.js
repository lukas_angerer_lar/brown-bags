
(function (angular) {



	/////

	function MainController() {
		this.text = 'Hello World!';
	}

	angular.extend(MainController.prototype, {
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');
	})



	.directive('container', function () {
		return {
			templateUrl: 'container-directive.view.html',
			transclude: true,
			scope: {
				title: '=',
			},
		};
	})

	;

})(angular);
