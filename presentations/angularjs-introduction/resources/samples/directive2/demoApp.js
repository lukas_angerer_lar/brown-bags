
(function (angular) {



	/////

	function MainController() {
		this.text = 'Hello World!';
	}

	angular.extend(MainController.prototype, {
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');
	})



	.directive('demoDirective', function () {
		return {
			templateUrl: 'demo-directive.view.html',
			scope: {
				text: '=',
			},
			link: function (scope, element, attributes) {
				// in this context we are working with jqLite, not full jQuery
				element.find('button').on('click', function () {
					scope.$apply(function () {
						scope.text = element.find('input').eq(0).val();
					});
				});
			},
		};
	})

	;

})(angular);
