
(function (angular) {

	function MainController($scope, $route, $sce, $log) {
		this.plainText = 'Some normal text <stuff />';
		this.htmlText = 'Some <strong>HTML</strong> markup';
	}

	angular.extend(MainController.prototype, {
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider, $sceProvider) {
		$sceProvider.enabled(false);

		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');
	})

	;

})(angular);
