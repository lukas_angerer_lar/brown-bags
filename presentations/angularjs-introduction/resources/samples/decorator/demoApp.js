
(function (angular) {

	/////

	function LogDataService() {
		this.messages = ['first'];
	}

	angular.extend(LogDataService.prototype, {
		append: function (msg) {
			this.messages.push(msg);
		},
	});

	/////

	/////

	function MainController(LogData, $log) {
		this.messages = LogData.messages;

		$log.log("my message", this.messages.length);
	}

	angular.extend(MainController.prototype, {
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.service('LogData', LogDataService)

	.config(function($routeProvider, $provide) {
		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');


		$provide.decorator('$log', function ($delegate, LogData) {
			var origLog = $delegate.log;

			$delegate.log = function () {
				var args = Array.prototype.slice.call(arguments)
				LogData.append(args);
				origLog.apply(this, args);
			}

			return $delegate;
		});
	})

	;

})(angular);
