
(function (angular) {

	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider) {
		$routeProvider
			.when('/:name', {
				controller: function ($scope, $route) {
					$scope.greeting = "Hello Controller!";
					$scope.name = $route.current.params.name;
				},
				template: '<div>{{ greeting }} {{ name }}</div>',
			})
			.otherwise('/Test');
	})

	.run(function ($rootScope) {
		$rootScope.greeting = 'Hello World!';
	})

	;

})(angular);
