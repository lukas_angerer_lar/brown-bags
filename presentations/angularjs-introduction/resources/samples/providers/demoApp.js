
(function (angular) {

	/////

	// HelloWorld Service Implementation
	function HelloWorld1() {
		this.text = 'Hello World from HelloWorld1 Service';
	}

	/////

	// HelloWorld Factory Implementation
	function HelloWorld2Factory() {
		return {
			text: 'Hello World from HelloWorld2 Factory Instance',
		};
	}

	/////

	// HelloWorld Provider Implementation
	function HelloWorld3Provider() {
		this.name = 'World';
	}

	angular.extend(HelloWorld3Provider.prototype, {
		setName: function (name) {
			this.name = name;
		},
		$get: function () {
			return {
				text: 'Hello ' + this.name + ' from HelloWorld3 Provider Instance',
			};
		},
	})

	/////

	function MainController(HelloWorld1, HelloWorld2, HelloWorld3) {
		this.values = [
			HelloWorld1.text,
			HelloWorld2.text,
			HelloWorld3.text,
		];
	}

	angular.extend(MainController.prototype, {
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.service('HelloWorld1', HelloWorld1)

	.factory('HelloWorld2', HelloWorld2Factory)

	// watch for the automatic 'Provider' suffix
	.provider('HelloWorld3', HelloWorld3Provider)

	.config(function($routeProvider, HelloWorld3Provider) {
		HelloWorld3Provider.setName('Test Osteron');

		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');
	})

	;

})(angular);
