
(function (angular) {

	window.demoApp = angular.module('demoApp', [])

	.run(function ($rootScope) {
		$rootScope.greeting = 'Hello World!';
	})

	;

})(angular);
