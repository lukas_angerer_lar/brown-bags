
(function (angular) {

	function MainController($scope, $route) {
		this.greeting = "Hello Controller!";
		this.name = $route.current.params.name;

		this.alert();
	}

	angular.extend(MainController.prototype, {
		alert: function () {
			alert('instantiating the controller');
		},
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider) {
		$routeProvider
			.when('/:name', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/Test');
	})

	.run(function ($rootScope) {
		$rootScope.greeting = 'Hello World!';
	})

	;

})(angular);
