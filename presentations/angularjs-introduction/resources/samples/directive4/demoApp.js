
(function (angular) {

	function CheckboxController($scope, $element, $attrs, $transclude) {
		var that = this;

		that.binding = {};

		angular.forEach(that.options, function (opt) {
			Object.defineProperty(that.binding, opt, {
				get: function () {
					return that.values.indexOf(opt) >= 0;
				},
				set: function (val) {
					if (val) {
						that.values.push(opt);
					} else {
						that.values.splice(that.values.indexOf(opt), 1);
					}
				},
				enumerable: true,
			});
		});
	}

	/////

	function MainController() {
		this.options = ['SC', 'SC2', 'C&C', 'AoE'];
		this.values = ['SC2'];
	}

	angular.extend(MainController.prototype, {
	});



	window.demoApp = angular.module('demoApp', [
		'ngRoute'
	])

	.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				controller: MainController,
				controllerAs: 'ctlMain',
				templateUrl: 'main.view.html',
			})
			.otherwise('/');
	})



	.directive('checkboxes', function () {
		return {
			templateUrl: 'checkboxes-directive.view.html',
			bindToController: true,
			controllerAs: 'ctlCheckboxes',
			scope: {
				options: '=',
				values: '=',
			},
			controller: CheckboxController,
		};
	})

	;

})(angular);
