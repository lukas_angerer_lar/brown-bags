
var i, someObjects = [
	{}, // Object()
	42, // Number()
	'test', // String()
	function f() {}, // Function()
	/regex/, // Regex
];

for (i = 0; i < someObjects.length; i++) {
	console.log(someObjects[i].constructor);
}


// it is a good practice to start your constructor functions with an uppercase letter

function Foo() {
	console.log(this.constructor);
	this.x = 'x';
	this.y = 'y';
}

var x = new Foo();
console.log(x.constructor);
