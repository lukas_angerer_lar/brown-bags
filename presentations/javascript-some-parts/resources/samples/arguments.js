
// apply with  Array.prototype.slice

(function () {

	function foo(a, b, c) {
		console.log('individual arguments', a, b, c);
		console.log('arguments magic variable', arguments);
	}

	foo(1, 2, 3);

	function bar(a, b, c, d) {
		// works
		foo.apply(this, arguments);

		// doesn't work because arguments is not an array
		foo.apply(this, arguments.slice(1));

		// converting the arguments object into an array helps in such a case
		//var args = Array.prototype.slice.call(arguments, 1);
		//foo.apply(this, args);
	}

	bar(1, 2, 3, 4);

})();

// properly wrapping a function => monkey patching

(function () {

	var origLog = console.log;
	console.log = function() {
		var args = Array.prototype.slice.call(arguments);
		args.unshift('customized logging enabled');

		origLog.apply(this, args);
	};

	console.log('test', 42);

	// reset to original logging
	console.log = origLog;

})();
