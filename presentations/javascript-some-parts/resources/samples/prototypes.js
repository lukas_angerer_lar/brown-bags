
function Foo() {
}

Foo.prototype.test = 42;

var f = new Foo();
console.log(f);

f.some = 'some';
console.log(f);

delete Foo.prototype.test;
console.log(f);

Foo.prototype.name = "The Foo";
console.log(f);

f.name = "The F";
console.log(f);

delete Foo.prototype.name;
console.log(f);

// Looking at the prototype itself

console.log(Foo.prototype);
console.log(Foo.prototype.constructor);

// Now, let's add another layer

function Bar() {
}

Bar.prototype.barMaid = "Bar Maid";

Foo.prototype = new Bar();
Foo.prototype.fooBar = "The Bar with the Foo";

f = new Foo();

console.log(f);
console.log(f instanceof Foo);
console.log(f instanceof Bar);
console.log(f.constructor); // Bar() ... jupp. deal with it. ***

Foo.prototype.constructor = Foo;
console.log(f.constructor); // Foo() ... :(
