
//////////////////
// function basics

// "classic" function declaration

function foo() {
	console.log('foo');
}

foo();

// anonymous function declaration

var bar = function () {
	console.log('bar');
};

bar();

// name of the function vs. name of the variable

var foobar = function barfoo() {
	console.log('barfoo');
};

console.log(foobar.name);

// other differences in declaration styles

// call function defined "later" in the lexical scope
// (FYI: don't do this)
alpha();

function alpha() {
	console.log('alpha');
}

// with a function declaration as a variable, this doesn't work
beta();

var beta = function () {
	console.log('beta');
};

///////////
// closures

function foo() {
	var value = 42,
		name = "foo",
		test = 0;

	function bar() {
		var name = "bar";
		console.log('bar says: ', value, name, test);
		test++;
	}

	bar();
	console.log('foo says: ', value, name, test);
	value = 666;
	bar();
	console.log('foo says: ', value, name, test);
}

foo();

///////
// IIFE

(function ($) {

	// explicitly creating a global
	window.myGlobal = 42;

	// using locally aliased global from IIFE argument
	console.log('IIFE jQuery version: ', $().jquery);

})(jQuery);
