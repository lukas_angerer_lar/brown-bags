
//////////
// this-es

function foo() {
	console.log('foo says this is', this);
}

// stand-alone
foo();

// calling on an object
obj = {
	foo: foo,
};

obj.foo();

// yeah .....
var bar = obj.foo;
bar();

// using foo.call to provide the this context
foo.call("this");

//////////////////
// this in closure

(function () {

	function foo() {
		var that = this;

		function bar() {
			console.log('bar says this is', this);
			console.log('bar says that is', that);
		}

		console.log('foo says this is', this);
		bar();
	}

	foo.call("test");

})();
