
(function ($) {

	function AsyncMsg(msg, delay, fail) {
		var deferred = $.Deferred();

		window.setTimeout(function () {
			if (fail === true) {
				deferred.reject('I broke my promise :( [' + msg + ']');
			} else {
				deferred.resolve(msg);
			}
		}, delay || 1000);

		return deferred;
	}

	window.AsyncMsg = AsyncMsg;

})(jQuery);

// simple promise

AsyncMsg('Hello Promising World!')
	.then(function (msg) {
		console.log('arrived', msg);
	});

// chaining

AsyncMsg('First Message')
	.then(function (msg) {
		console.log('first', msg);
		return AsyncMsg('Second Message');
	})
	.then(function (msg) {
		console.log('second', msg);
	});

// failing promises

AsyncMsg('Do. Or do not. There is no try.', 2000, true)
	.then(function (msg) {
		console.log('success!', msg);
	})
	.fail(function (reason) {
		console.log('fail!', reason);
	});

// failing in a chain

AsyncMsg('First Message', 1000, true)
	.then(function (msg) {
		console.log('first', msg);
		return AsyncMsg('Second Message');
	})
	.then(function (msg) {
		console.log('second', msg);
	})
	.fail(function (reason) {
		console.log('failed', reason);
	})
	.always(function () {
		console.log('cleaning up');
	});

// aggregating and wrapping

$.when(42)
	.then(function (val) {
		console.log('numbers are not promises, but this one is', val);
	});

$.when(
		AsyncMsg('First Message', 1000),
		AsyncMsg('Second Message', 2000),
		AsyncMsg('Third Message', 3000)
	)
	.then(function (first, second, third) {
		console.log('results');
		console.log(first);
		console.log(second);
		console.log(third);
	});
