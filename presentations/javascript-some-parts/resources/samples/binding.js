
///////////////
// binding this

function foo() {
	console.log('foo says this is', this);
}

var bar = foo.bind('test');
bar();
// bar is simply a wrapper that calls foo; it's this is irrelevant
bar.call(42);

// the same effect as with bind can be achieved "manually"

bar = function () {
	console.log('in bar, this is', this);
	foo.call('test');
};
bar();
bar.call(42);


///////////////
// binding arguments

function add(a, b) {
	return a + b;
}

add(5, 3);

// binding with null (-> window) as this and a "a = 42"
var partial = add.bind(null, 42);
partial(3);

partial = add.bind(null, 2, 8);
partial();

// the problem with binding arguments in order

function subtract(a, b) {
	return a - b;
}

var subtractFrom100 = subtract.bind(null, 100);
subtractFrom100(45);

// binding only argument b is not possible with bind
var subtract20 = function (a) {
	return subtract(a, 20);
};
subtract20(100);

