
function Foo() {
	this.definedInConstructor = 'yes';
}

Foo.prototype.definedInPrototype = 'yes';

var f = new Foo();

f.hasOwnProperty('definedInConstructor');
f.hasOwnProperty('definedInPrototype');

f.definedInObject = 'yes';
f.hasOwnProperty('definedInObject');

//////

for (var p in f) {
	console.log(p);
}

for (var p in f) {
	if (f.hasOwnProperty(p)) {
		console.log(p);
	}
}

var a = [1, 2, 3];
a.testing = 42;

for (var p in a) {
	console.log(p);
}

///////

var o = {};
Object.defineProperties(o, {
	'static': { value: 'i cannot be modified, enumerated or deleted'},
	'enumerable': { enumerable: true, value: 'i cannot be modified or deleted'},
	'writable': { enumerable: true, writable: true, value: 'i am almost like a "normal" property'},
	'default': { configurable: true, enumerable: true, value: 'normal'},
});

for (var p in o) {
	console.log('property', p);
}

console.log(o.static);
console.log('deleting', delete o.static);
o.static = 'modified';
console.log('modified?', o.static);


o.writable = 'modified';
console.log('modified?', o.writable);
console.log('deleting', delete o.writable);


console.log('deleting', delete o.default);
for (var p in o) {
	console.log('property', p);
}


// and now for the getters and setters

o = {};

(function () {
	var storage = {
		readonly: 42,
		writable: undefined,
	};

	Object.defineProperties(o, {
		'readonly': {
			enumerable: true,
			get: function() {
				console.log('getting readonly value');
				return storage.readonly;
			},
		},
		'writable': {
			enumerable: true,
			get: function() {
				console.log('getting writable value');
				return storage.writable;
			},
			set: function(val) {
				console.log('setting writable value');
				storage.writable = val;
			},
		},
	});
})();

