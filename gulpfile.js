var through2 = require('through2');
var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var merge = require('merge-stream');
var gutil = require('gulp-util');
var tap = require('gulp-tap');
var concat = require('gulp-concat');
var indent = require('gulp-indent');
var rename = require("gulp-rename");
var twig = require('gulp-twig');
var sass = require('gulp-sass');
var symlink = require('gulp-symlink');
var del = require('del');
var config = require('./build-config.json');
var watch = false;
var watching = {};

for (var i = 2; i < process.argv.length; i++)
{
	if (process.argv[i] === '--watch') {
		watch = true;
		break;
	}
}

function watchOnce (glob, tasks) {
	var key = (typeof glob === 'string' ? glob : glob.join('|'));
	if (watch && watching[key] === undefined)
	{
		gutil.log('watching:', glob);
		watching[key] = true;
		gulp.watch(glob, tasks);
	}
}

function getFolders (dir) {
	return fs.readdirSync(dir)
		.filter(function(file) {
			return fs.statSync(path.join(dir, file)).isDirectory() && file !== 'resources';
		});
}

function twigWrap (template, data, contentName, pipeFn) {
	return through2.obj(function (file, err, callback) {
		var content = file.contents.toString('utf-8');

		contentName = contentName || 'content';
		data[contentName] = content;

		stream = gulp.src(template)
			.pipe(twig({ data: data	}));

		stream = pipeFn(stream);

		stream.on('end', function () {
			gutil.log('Twig wrap complete for "' + template + '"');
			callback(null);
		});
	});
}

function buildSlides (name) {
	return function () {
		var basePath = path.join(config.presentations.path, name);
		var presentationConfig = require('./' + path.join(basePath, 'config.json'));

		var slides = getFolders(basePath).map(function (folder) {

			watchOnce(path.join(basePath, folder, '*.html'), [name + '-slides']);

			return gulp.src(path.join(basePath, folder, '*.html'))
				.pipe(twig({ data: { bundle: presentationConfig }}))
				.pipe(concat(folder + '.html'))
				//.pipe(indent({ tabs: true, amount: 1 }))
				.pipe(
					twigWrap(basePath + '/../vertical.html.twig', { slideId: folder }, null, function (stream) {
						return stream
							.pipe(rename(folder + '.html'))
							.pipe(gulp.dest('temp/' + name));
					})
				)
				.on('end', function () {
					gutil.log("Completed slides collection " + folder + " (for " + name + ")");
				});
		});

		watchOnce(path.join(basePath, '*.html'), [name + '-slides']);

		var slidesRaw = gulp.src(path.join(basePath, '*.html'))
			.pipe(gulp.dest('temp/' + name));

		return merge(slides, slidesRaw);
	};
}

function buildPresentation (name) {

	return function () {
		gutil.log('Building presentation "' + name + '"');
		var basePath = path.join(config.presentations.path, name);
		var presentationConfig = require('./' + path.join(basePath, 'config.json'));

		watchOnce(path.join('temp', name, '*.html'), [name + '-presentation']);

		return gulp.src(path.join('temp', name, '*.html'))
			.pipe(concat('slides.html'))
			//.pipe(indent({ tabs: true, amount: 4 }))
			.pipe(
				twigWrap(basePath + '/../presentation.html.twig', {
					config: config,
					bundle: presentationConfig
				}, 'slides', function (stream) {
					return stream
						.pipe(rename('index.html'))
						.pipe(gulp.dest('build/' + name));
				})
			);
	};
}

function copyResources (name) {

	var basePath = path.join(config.presentations.path, name);
	var presentationConfig = require('./' + path.join(basePath, 'config.json'));

	watchOnce(path.join(basePath, 'resources/**'), [name + '-resources']);

	return function () {
		return gulp.src(path.join(basePath, 'resources/**'))
			.pipe(gulp.dest(path.join('build', name, 'resources')));
	};
}

var presentations = getFolders(config.presentations.path);
presentations.map(function (name) {
	gulp.task(name + '-slides', buildSlides(name));
	gulp.task(name + '-presentation', [name + '-slides'], buildPresentation(name));
	gulp.task(name + '-resources', copyResources(name));
	gulp.task(name, [name + '-presentation', name + '-resources'], function () {
		if (!watch) {
			del('temp/' + name);
		}
	});
});


gulp.task('default', presentations.concat(['reveal', 'themes']), function () {
	del('temp');
});

gulp.task('reveal', function () {
	gulp.src(path.join(config.reveal.path, 'js/reveal.js'))
		.pipe(gulp.dest('build/js'));

	gulp.src(path.join(config.reveal.path, 'plugin/**'))
		.pipe(gulp.dest('build/plugin'));

	gulp.src(path.join(config.reveal.path, 'lib/**'))
		.pipe(gulp.dest('build/lib'));
});

gulp.task('themes', function () {
	watchOnce('themes/*.scss', ['themes']);

	gulp.src(['themes/*.scss', path.join(config.reveal.path, 'css/reveal.scss')])
		.pipe(sass())
		.pipe(gulp.dest('build/css'));

	gulp.src(path.join(config.reveal.path, 'css/print/**'))
		.pipe(gulp.dest('build/css/print'));
});

gulp.task('clean', function (done) {
	del(['temp', 'build'], function () { done(); });
});

gulp.task('dummy', function () {
	gutil.log("watching", watch);
});
